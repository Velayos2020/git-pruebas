***Settings***
Documentation   Prueba Regresiva Automática busqueda google
Library     SeleniumLibrary
***Test Cases***
C001 Hacer click en el botón de búsqueda sin escribir palabras en Google
    Open Browser        https://www.google.com/     chrome
    Wait Until Element is Visible     xpath=//*[@id="hplogo"]
    Input Text      xpath=//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input   casos de prueba
    Click Element   xpath=//*[@id="lga"]
    Click Element   xpath=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be   casos de prueba - Buscar con Google
    Page Should Contain     casos de prueba
    Close Browser

C002 Hacer click en el botón de búsqueda sin escribir palabras en Google
    Open Browser        https://www.google.com/     chrome
    Wait Until Element is Visible     xpath=//*[@id="hplogo"]
    Click Element   xptah=//*[@id="tsf"]/div[2]/div[1]/div[3]/center/input[1]
    Title Should Be   Google
    Close Browser

C003 Abrir Navegador
    Open Browser        https://www.google.com/     chrome
    Wait Until Element is Visible     xpath=//*[@id="hplogo"]
    Close Browser
